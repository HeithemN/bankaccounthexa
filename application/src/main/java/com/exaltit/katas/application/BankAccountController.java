package com.exaltit.katas.application;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exaltit.katas.domain.api.BankAccountService;
import com.exaltit.katas.domain.model.BankAccount;
import com.exaltit.katas.domain.model.BankAccountStatement;
import com.exaltit.katas.domain.model.StatementLine;

@RestController
@RequestMapping(path = "/bankaccounts")
public class BankAccountController {
	
	private final BankAccountService bankAccountService;
	
	@Autowired
	public BankAccountController(BankAccountService bankAccountService) {
		this.bankAccountService = bankAccountService;
	}
	
	
	
	@GetMapping(path="/statements/{id}",  produces = "application/json")
    public List<StatementLine> getBankAccountHistory(@PathVariable long id) 
    {
		BankAccount targetBanAccount = bankAccountService.getBankAccountById(id);
        return bankAccountService.getBankAccountHistory(targetBanAccount);
    }
	
	
	@RequestMapping(value = "/deposit",method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
    BankAccount makeDeposit(@RequestParam(name = "id") String Id, @RequestParam(name = "amount") String amount ) {
		BankAccount targetBanAccount = bankAccountService.getBankAccountById(Long.valueOf(Id).longValue());
		
		bankAccountService.makeDeposit(targetBanAccount, BigDecimal.valueOf(Double.valueOf(amount)));

        return targetBanAccount;
    }
	
/*	
	private class MakeDepositRequest{
		private  Long bankAccountId;
		private BigDecimal amount;
		public Long getBankAccountId() {
			return bankAccountId;
		}
		public void setBankAccountId(Long bankAccountId) {
			this.bankAccountId = bankAccountId;
		}
		public BigDecimal getAmount() {
			return amount;
		}
		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}
		
		
	}
*/
}
