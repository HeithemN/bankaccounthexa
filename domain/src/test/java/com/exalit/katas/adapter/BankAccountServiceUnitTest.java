package com.exalit.katas.adapter;

import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;

import com.exaltit.katas.domain.adapter.DomainBankAccountService;
import com.exaltit.katas.domain.model.BankAccount;
import com.exaltit.katas.domain.model.BankAccountStatement;
import com.exaltit.katas.domain.spi.BankAccountRepository;

/**
 * 
 * @author HEITHEMN
 *
 */

@RunWith(JUnit4.class)
public class BankAccountServiceUnitTest {
	
	private static BankAccountRepository bankAccountRepository;
	private static DomainBankAccountService bankAccountService;

	@BeforeClass
	public static void setUp() {
		bankAccountRepository =  mock (BankAccountRepository.class);
		bankAccountService = new DomainBankAccountService(bankAccountRepository);
	}
	
	@org.junit.Test
	public void shouldDeposit_withSuccess() {
		
	BankAccount	targetBanAccount = new BankAccount(1, "HEITHEM", new BigDecimal(0));
	targetBanAccount.setStatement(new BankAccountStatement());
	bankAccountService.makeDeposit(targetBanAccount, new BigDecimal(10));
	
	assertEquals(targetBanAccount.getBalance(), new BigDecimal (10));
		
	}
}
