package com.exaltit.katas.domain.model;

import java.io.PrintStream;
import java.math.BigDecimal;

public class StatementLine {
	
	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 * @return the currentBalance
	 */
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}

	private Transaction transaction;
	private  BigDecimal currentBalance;
	
	
	public StatementLine (Transaction transaction, BigDecimal currentBalance) {
		
		this.transaction = transaction;
		this.currentBalance = currentBalance;
		
	}
	
	public void print (PrintStream printer) {
		
		this.transaction.print (printer, currentBalance);
	}

}
