package com.exaltit.katas.domain.model;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class BankAccountStatement {
	
	
	/**
	 * @return the statementLines
	 */
	public List<StatementLine> getStatementLines() {
		return statementLines;
	}


	public static final String STATEMENT_HEADER = "date       | credit   | debit    | balance";

	private List<StatementLine> statementLines = new LinkedList<StatementLine>();
	
	
	public void addStatementLine (Transaction transaction, BigDecimal currentBalance) {
		
		statementLines.add(0, new StatementLine(transaction, currentBalance));
		
	}
	
	
	public void print(PrintStream printer) {
		printer.println(STATEMENT_HEADER);
		statementLines.stream().forEach((s) -> s.print(printer));
	}

}
