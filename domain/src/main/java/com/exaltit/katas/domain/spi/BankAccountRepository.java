package com.exaltit.katas.domain.spi;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.exaltit.katas.domain.model.BankAccount;
import com.exaltit.katas.domain.model.BankAccountStatement;
import com.exaltit.katas.domain.model.StatementLine;

public interface BankAccountRepository {
	
	Optional<BankAccount> findById(long id);

    void saveMoney(BankAccount targetBanAccount, BigDecimal amount);
    
    void withdrawMoney(BankAccount targetBanAccount, BigDecimal amount);
    
    List<StatementLine> getBankAccountHistory (BankAccount targetBanAccount);

}
