package com.exaltit.katas.domain.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import com.exaltit.katas.domain.model.BankAccount;
import com.exaltit.katas.domain.model.BankAccountStatement;
import com.exaltit.katas.domain.model.StatementLine;

public interface BankAccountService {
	
	
	BankAccount getBankAccountById(long id);
	
	void makeDeposit (BankAccount targetBanAccount,BigDecimal amount);
	
	void makeWithDrawal(BankAccount targetBanAccount, BigDecimal amount);
	
	
	List<StatementLine> getBankAccountHistory (BankAccount targetBanAccount);

}
