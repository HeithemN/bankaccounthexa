package com.exaltit.katas.domain.model;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;



public class Transaction {
	
	private static final DateTimeFormatter DATE_FORMAT =  DateTimeFormatter.ofPattern("dd/MM/YYYY");
	private static final DecimalFormat decimalFormat = new DecimalFormat("#.00");
	
	private static final String EMPTY_VALUE = "          ";
	
	
	private BigDecimal amount;
	private LocalDateTime date;
	
	
	public Transaction (BigDecimal amount,LocalDateTime date ) {
		this.amount = amount;
		this.date = date;
		
	}
	
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @return the date
	 */
	public LocalDateTime getDate() {
		return date;
	}

	public void print(PrintStream printer, BigDecimal currentBalance) {
		StringBuilder builder = new StringBuilder();
		
		builder.append(date.format(DATE_FORMAT));
		builder.append(" |");
		
		addValueTo(builder);
		
		builder.append("| ")
		   .append(decimalFormat.format(amount));
		
		
		printer.println(builder.toString());
	}
	
	private void addValueTo(StringBuilder builder) {
		if (amount.compareTo(new BigDecimal(0)) > 0) {
			addCreditTo(builder);
		} else {
			addDebitTo(builder);
		}
	}
	
	private void addCreditTo(StringBuilder builder) {
		builder.append(valueToString())
				.append("|")
				.append(EMPTY_VALUE);
	}
	
	private void addDebitTo(StringBuilder builder) {
		builder.append(EMPTY_VALUE)
			   .append("|")
			   .append(valueToString());
	}
	
	private String valueToString() {
		String formattedValue = " " + decimalFormat.format (amount.abs());
		
		return StringUtils.rightPad(formattedValue, 10);
	}
	
	
	

}
