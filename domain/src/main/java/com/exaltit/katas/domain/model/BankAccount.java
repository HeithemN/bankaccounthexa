package com.exaltit.katas.domain.model;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.lang.NonNull;


public class BankAccount {
	
	@NonNull
	private long id;
	
	private String owner;
	private BigDecimal balance;
    private BankAccountStatement statement;
	
	/**
	 * @param statement the statement to set
	 */
	public void setStatement(BankAccountStatement statement) {
		this.statement = statement;
	}
	/**
	 * @return the statement
	 */
	public BankAccountStatement getStatement() {
		return statement;
	}
	public BankAccount(long id, String owner, BigDecimal balance) {
		super();
		this.id = id;
		this.owner = owner;
		this.balance = balance;
	}
	public BankAccount() {
	}
	
	public  BankAccount(BankAccountStatement statement) {
		this.statement = statement;
	}
	
	public void deposit (BigDecimal amount, LocalDateTime date) {
		saveTransaction( amount, date);
	}
	
	public void withdrawal(BigDecimal amount, LocalDateTime date) {
		saveTransaction(amount.negate(), date);
	}
	
	public void printStatement(PrintStream printer) {
		statement.print(printer);
	}
	
	private void saveTransaction (BigDecimal amount, LocalDateTime date) {		
		
		balance = balance.add(amount);
		Transaction transaction = new Transaction(amount, date);
		if (statement == null) {
			statement = new BankAccountStatement();
		}
		statement.addStatementLine (transaction,  balance);
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}
	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	
	

}
