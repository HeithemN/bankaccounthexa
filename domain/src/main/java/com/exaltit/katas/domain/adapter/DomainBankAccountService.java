package com.exaltit.katas.domain.adapter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.exaltit.katas.domain.api.BankAccountService;
import com.exaltit.katas.domain.model.BankAccount;
import com.exaltit.katas.domain.model.BankAccountStatement;
import com.exaltit.katas.domain.model.StatementLine;
import com.exaltit.katas.domain.spi.BankAccountRepository;
/**
 * 
 * @author HEITHEMN
 *
 */
public class DomainBankAccountService implements BankAccountService {
	
	private final BankAccountRepository bankAccountRepository;
	
	public DomainBankAccountService (BankAccountRepository bankAccountRepository) {
		this.bankAccountRepository = bankAccountRepository;
	}
	
	


	public BankAccount getBankAccountById(long id) {
		Optional <BankAccount> bankAccount =	bankAccountRepository.findById(id);
		if (bankAccount.isPresent()) {
			return bankAccount.get();
		}
		else {
			return null;
		}
	}
	
	public void makeDeposit (BankAccount targetBanAccount, BigDecimal amount) {
		
		targetBanAccount.deposit(amount,LocalDateTime.now());
	}

	public void makeWithDrawal(BankAccount targetBanAccount,BigDecimal amount) {
		bankAccountRepository.withdrawMoney( targetBanAccount,amount);
		
	}



	public List<StatementLine> getBankAccountHistory(BankAccount targetBanAccount) {
		return bankAccountRepository.getBankAccountHistory(targetBanAccount);
	}

}
