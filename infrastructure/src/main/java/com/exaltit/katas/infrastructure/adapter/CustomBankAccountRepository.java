package com.exaltit.katas.infrastructure.adapter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Repository;

import com.exaltit.katas.domain.model.BankAccount;
import com.exaltit.katas.domain.model.BankAccountStatement;
import com.exaltit.katas.domain.model.StatementLine;
import com.exaltit.katas.domain.spi.BankAccountRepository;

/**
 * A custom repository class for in-memory data persistence 
 * @author HEITHEMN
 *
 */
@Repository
public class CustomBankAccountRepository implements BankAccountRepository {
	
	
	private static List<BankAccount> bankAccounts = new ArrayList<BankAccount>();
	
	
	static
    {
		bankAccounts.add(new BankAccount(1, "HEITHEM", new BigDecimal(0)));
		bankAccounts.add(new BankAccount(2, "JULIEN", new BigDecimal(0)));
		
    }

	/**
	 * Returns an instance of BankAccount by a given {id}
	 * @param id is a technical identifier of the bank account that we want to search for
	 * @throws NoBankAccountFoundException
	 */
	public Optional<BankAccount> findById(long id)  {
		
		return bankAccounts.stream().filter(b -> b.getId() == id).findFirst();
	}

	public void saveMoney(BankAccount targetBanAccount, BigDecimal amount) {
		
		targetBanAccount.deposit(amount, LocalDateTime.now());		

	}

	public void withdrawMoney(BankAccount targetBanAccount,BigDecimal amount) {
		//return  false;

	}

	public List<StatementLine> getBankAccountHistory(BankAccount targetBanAccount) {
		targetBanAccount.printStatement(System.out);
		 
		 return  targetBanAccount.getStatement().getStatementLines();
	}

}
