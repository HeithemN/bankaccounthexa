package com.exaltit.katas.launcher.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.exaltit.katas.application.BankAccountController;
import com.exaltit.katas.domain.adapter.DomainBankAccountService;
import com.exaltit.katas.domain.api.BankAccountService;
import com.exaltit.katas.domain.model.BankAccount;
import com.exaltit.katas.domain.spi.BankAccountRepository;
import com.exaltit.katas.infrastructure.adapter.CustomBankAccountRepository;



@Configuration
@EntityScan(basePackageClasses = BankAccount.class)
@ComponentScan(basePackageClasses = BankAccountController.class)
@ComponentScan(basePackageClasses = BankAccountRepository.class)
public class BeanConfiguration {
	
	
	@Bean
	BankAccountRepository bankAccountRepository() {
        return new CustomBankAccountRepository();
    }
	
	@Bean
	BankAccountService bankAccountService(BankAccountRepository bankAccountRepository) {
        return new DomainBankAccountService(bankAccountRepository);
    }

}
